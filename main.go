package main

import (
	"image"
	"os"
	"path/filepath"
	"strings"

	"github.com/docopt/docopt-go"
	"github.com/kovetskiy/lorg"
	"github.com/reconquest/karma-go"
	"gitlab.com/seletskiy/imgp/processing"

	_ "image/jpeg"
	_ "image/png"
)

var version = "1.0"

var usage = `imgp - image processor with face detection.

Usage:
  imgp -h | --help
  imgp [options] --classifier=<path> --spec=<spec> <input> <output>

Options:
  -h --help            Show this help.
  --spec <spec>        Spec used to process image.
  --classifier <path>  Path to classifier for face recognizer.
  --watermark <path>   Path to optional watermark.
  --watermark-x <pos>  Coord for watermark in resized image. Use negative for
                        specifying offset from right.
  --watermark-y <pos>  Coord for watermark in resized image. Use negative for
                        specifying offset from bottom.
`

type Opts struct {
	Spec       string
	Classifier string
	Watermark  string
	WatermarkX int
	WatermarkY int

	Input  string
	Output string
}

var (
	log *lorg.Log
)

func main() {
	log = lorg.NewLog()

	log.SetLevel(lorg.LevelDebug)
	log.SetIndentLines(true)
	log.SetFormat(lorg.NewFormat(
		`${time:2006-01-02T15:04:05Z07:00.000} ${level:[%s]\::right:true} ${prefix}%s`,
	))

	args, err := docopt.ParseArgs(usage, nil, "imgp "+version)
	if err != nil {
		panic(err)
	}

	var opts Opts

	err = args.Bind(&opts)
	if err != nil {
		log.Fatal(err)
	}

	options, err := processing.NewOptions(opts.Spec)
	if err != nil {
		log.Fatal(err)
	}

	recognizer, err := NewFaceRecognizer(opts.Classifier)
	if err != nil {
		log.Fatal(err)
	}

	var watermark processing.Watermark

	processing := processing.New(
		*options,
		recognizer,
		log,
	)

	if opts.Watermark != "" {
		watermarkFile, err := os.Open(opts.Watermark)
		if err != nil {
			log.Fatal(err)
		}

		watermark.Image, _, err = image.Decode(watermarkFile)
		if err != nil {
			log.Fatal(err)
		}

		watermark.X = opts.WatermarkX
		watermark.Y = opts.WatermarkY

		processing.Watermarks["watermark"] = &watermark
	}

	err = processing.LoadFile(opts.Input)
	if err != nil {
		log.Fatal(err)
	}

	processing.SetFormat(strings.TrimPrefix(filepath.Ext(opts.Output), "."))

	output, err := os.Create(opts.Output)
	if err != nil {
		log.Fatal(
			karma.Format(
				err,
				"unable to create output file: %q",
				opts.Output,
			),
		)
	}

	err = processing.Process(output)
	if err != nil {
		log.Fatal(
			karma.Format(
				err,
				"unable to write output file: %q",
				opts.Output,
			),
		)
	}
}
