package processing

import (
	"image/color"

	colors "gopkg.in/go-playground/colors.v1"
)

var (
	namedColors = map[string]string{
		"white":   "FFFFFF",
		"silver":  "C0C0C0",
		"gray":    "808080",
		"black":   "000000",
		"red":     "FF0000",
		"maroon":  "800000",
		"yellow":  "FFFF00",
		"olive":   "808000",
		"lime":    "00FF00",
		"green":   "008000",
		"aqua":    "00FFFF",
		"teal":    "008080",
		"blue":    "0000FF",
		"navy":    "000080",
		"fuchsia": "FF00FF",
		"purple":  "800080",
	}
)

func parseColorName(name string) (*color.RGBA, error) {
	if color, ok := namedColors[name]; ok {
		name = color
	}

	rgba, err := colors.ParseHEX("#" + name)
	if err != nil {
		return nil, err
	}

	return &color.RGBA{
		R: rgba.ToRGB().R,
		G: rgba.ToRGB().G,
		B: rgba.ToRGB().B,
		A: 255,
	}, nil
}
