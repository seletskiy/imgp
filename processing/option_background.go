package processing

import "github.com/reconquest/karma-go"
import "image/color"

type OptionBackground = color.RGBA

var (
	OptionBackgroundTransparent = color.RGBA{0, 0, 0, 0}
)

func NewOptionBackground(spec string) (*OptionBackground, error) {
	if spec == "transparent" {
		return &OptionBackgroundTransparent, nil
	}

	rgba, err := parseColorName(spec)
	if err != nil {
		return nil, karma.Format(
			err,
			"{option:background} unable to color: %q",
			spec,
		)
	}

	return rgba, nil
}
