package processing

import (
	"strconv"

	"github.com/reconquest/karma-go"
)

type OptionInt int64

func NewOptionInt(spec string) (*OptionInt, error) {
	value, err := strconv.ParseInt(spec, 10, 0)
	if err != nil {
		return nil, karma.Format(
			err,
			"{option:int} unable to parse spec as int: %q",
			spec,
		)
	}

	option := OptionInt(value)

	return &option, nil
}

func (option OptionInt) Int64() int64 {
	return int64(option)
}
