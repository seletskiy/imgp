package processing

import (
	"image"
	"io/ioutil"

	"github.com/esimov/pigo/core"
	"github.com/nfnt/resize"
	"github.com/reconquest/karma-go"
)

type FaceRecognizerPigo struct {
	classifier *pigo.Pigo
}

func NewFaceRecognizerPigo(classifier string) (*FaceRecognizerPigo, error) {
	api := pigo.NewPigo()

	packet, err := ioutil.ReadFile(classifier)
	if err != nil {
		return nil, karma.Format(
			err,
			"{face:pigo} unable to read classifier file: %q",
			classifier,
		)
	}

	var recognizer FaceRecognizerPigo

	recognizer.classifier, err = api.Unpack(packet)
	if err != nil {
		return nil, karma.Format(
			err,
			"{face:pigo} error while unpacking classifier: %q",
			classifier,
		)
	}

	return &recognizer, nil
}

func (recognizer *FaceRecognizerPigo) Recognize(
	img image.Image,
) []image.Rectangle {
	scale := float64(img.Bounds().Dx()) / 256
	input := resize.Resize(256, 0, img, resize.NearestNeighbor)

	pixels := pigo.RgbToGrayscale(pigo.ImgToNRGBA(input))

	params := pigo.CascadeParams{
		MinSize:     20,
		MaxSize:     input.Bounds().Dy(),
		ShiftFactor: 0.1,
		ScaleFactor: 1.1,
	}

	detections := recognizer.classifier.RunCascade(
		pigo.ImageParams{
			Pixels: pixels,
			Rows:   input.Bounds().Dy(),
			Cols:   input.Bounds().Dx(),
			Dim:    input.Bounds().Dx(),
		},
		params,
	)

	detections = recognizer.classifier.ClusterDetections(detections, 0.2)

	if len(detections) == 0 {
		return nil
	}

	var best pigo.Detection

	for _, detection := range detections {
		if detection.Q > best.Q {
			best = detection
		}

		if detection.Q == best.Q {
			if detection.Scale > best.Scale {
				best = detection
			}
		}
	}

	return []image.Rectangle{
		image.Rect(
			int(scale*float64(best.Col-best.Scale/2)),
			int(scale*float64(best.Row-best.Scale/2)),
			int(scale*float64(best.Col+best.Scale/2)),
			int(scale*float64(best.Row+best.Scale/2)),
		),
	}
}
