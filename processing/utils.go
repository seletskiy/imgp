package processing

import (
	"image"
	"image/color"
)

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func fill(rgba *image.RGBA, color color.Color) {
	width, height := rgba.Bounds().Dx(), rgba.Bounds().Dy()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			rgba.Set(x, y, color)
		}
	}
}
