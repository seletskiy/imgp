package processing

import (
	"image"
)

type FaceRecognizer interface {
	Recognize(image.Image) []image.Rectangle
}
