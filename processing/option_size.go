package processing

import (
	"strconv"

	"github.com/reconquest/karma-go"
)

type OptionSize struct {
	value    float64
	relative bool
}

func NewOptionSize(spec string) (*OptionSize, error) {
	size := OptionSize{}

	value, err := strconv.ParseUint(spec, 10, 0)
	if err != nil {
		ratio, err := strconv.ParseFloat(spec, 0)
		if err != nil {
			return nil, karma.Format(
				err,
				"{option:size} size is not either int or float: %q",
				spec,
			)
		}

		size.value = ratio
		size.relative = true
	} else {
		size.value = float64(value)
	}

	return &size, nil
}

func (size OptionSize) Relative() bool {
	return size.relative
}

func (size OptionSize) Float64() float64 {
	return size.value
}

func (size OptionSize) Int64() int64 {
	return int64(size.value)
}
