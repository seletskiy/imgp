package processing

import "fmt"

type OptionGravity string

const (
	OptionGravityCenter OptionGravity = "center"
	OptionGravityFace                 = "face"
)

func NewOptionGravity(spec string) (*OptionGravity, error) {
	known := map[OptionGravity]bool{
		OptionGravityFace:   true,
		OptionGravityCenter: true,
	}

	option := OptionGravity(spec)

	if !known[option] {
		return nil, fmt.Errorf(
			"{option:gravity} unknown gravity type: %s",
			spec,
		)
	}

	return &option, nil
}
