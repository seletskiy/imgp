package processing

import "fmt"
import "strconv"

type OptionRadial = int

var (
	OptionRadialMax OptionRadial = 0
)

func NewOptionRadial(spec string) (*OptionRadial, error) {
	if spec == "max" {
		return &OptionRadialMax, nil
	}

	radius, err := strconv.Atoi(spec)
	if err != nil {
		return nil, fmt.Errorf(
			"{option:radial} unsupported radial value: %s (%s)",
			spec,
			err,
		)
	}

	return &radius, nil
}
