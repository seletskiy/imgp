package processing

import (
	"errors"
	"image/color"
	"strconv"
	"strings"

	"github.com/reconquest/karma-go"
)

type OptionBorder struct {
	Width uint64
	Style string // unused
	Color *color.RGBA
}

func NewOptionBorder(spec string) (*OptionBorder, error) {
	parts := strings.SplitN(spec, "_", 3)

	if len(parts) < 3 {
		return nil, errors.New(
			"{option:border} border option should be consisted of 3 parts",
		)
	}

	width, err := strconv.ParseUint(strings.TrimSuffix(parts[0], "px"), 10, 0)
	if err != nil {
		return nil, karma.Format(
			err,
			"{option:border} unable to parse width: %q",
			parts[0],
		)
	}

	rgba, err := parseColorName(parts[2])
	if err != nil {
		return nil, karma.Format(
			err,
			"{option:border} unable to color: %q",
			parts[2],
		)
	}

	return &OptionBorder{
		Width: width,
		Color: rgba,
	}, nil
}
