package processing

import (
	"image"
)

type Ellipsoid interface {
	SetCenter(image.Point)
	SetRadiusX(int)
	SetRadiusY(int)
}
