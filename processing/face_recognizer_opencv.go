// +build opencv

package processing

import (
	"fmt"
	"image"

	"gocv.io/x/gocv"
)

type FaceRecognizerOpenCV struct {
	classifier gocv.CascadeClassifier
}

func NewFaceRecognizerOpenCV(classifier string) (*FaceRecognizerOpenCV, error) {
	var recognizer FaceRecognizerOpenCV

	recognizer.classifier = gocv.NewCascadeClassifier()

	if !recognizer.classifier.Load(classifier) {
		return nil, fmt.Errorf(
			"{face:cv} unable to load classifier file: %q",
			classifier,
		)
	}

	return &recognizer, nil
}

func (recognizer *FaceRecognizerOpenCV) Recognize(
	image image.Image,
) []image.Rectangle {
	matrix, err := gocv.ImageToMatRGB(image)
	if err != nil {
		// we ignore error because it will fail only if we pass empty image
		return nil
	}

	return recognizer.classifier.DetectMultiScale(matrix)
}
