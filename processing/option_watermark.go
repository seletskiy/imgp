package processing

type OptionWatermark string

func NewOptionWatermark(spec string) (*OptionWatermark, error) {
	option := OptionWatermark(spec)

	return &option, nil
}

func (watermark OptionWatermark) Name() string {
	return string(watermark)
}
