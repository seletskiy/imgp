package processing

import (
	"image"
	"image/color"
	"math"
)

type Ellipse struct {
	Center  image.Point
	RadiusX int
	RadiusY int
}

func (ellipse *Ellipse) ColorModel() color.Model {
	return color.AlphaModel
}

func (ellipse *Ellipse) Bounds() image.Rectangle {
	return image.Rect(
		ellipse.Center.X-ellipse.RadiusX,
		ellipse.Center.Y-ellipse.RadiusY,
		ellipse.Center.X+ellipse.RadiusX,
		ellipse.Center.Y+ellipse.RadiusY,
	)
}

func (ellipse *Ellipse) At(x, y int) color.Color {
	xx := float64(x-ellipse.Center.X) + 0.5
	yy := float64(y-ellipse.Center.Y) + 0.5
	aa := float64(ellipse.RadiusX)
	bb := float64(ellipse.RadiusY)

	d := xx*xx/aa/aa + yy*yy/bb/bb
	if d < 1 {
		return color.Alpha{255}
	}

	t := 2 / math.Min(aa, bb)

	ee := (d - 1) / t

	if ee > 1 {
		return color.Alpha{0}
	}

	return color.Alpha{uint8((1 - ee) * 255)}
}
