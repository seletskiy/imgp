package processing

import (
	"strings"

	"github.com/reconquest/karma-go"
)

const (
	OptionNotSet = "<not set>"
)

type Options struct {
	Width      *OptionSize
	Height     *OptionSize
	Quality    *OptionInt
	Crop       *OptionCrop
	Gravity    *OptionGravity
	Radial     *OptionRadial
	Border     *OptionBorder
	Watermark  *OptionWatermark
	Background *OptionBackground
}

func NewOptions(spec string) (*Options, error) {
	specs := strings.Split(spec, ",")

	var (
		err     error
		options Options
	)

	for i := range specs {
		var (
			parts   = strings.SplitN(specs[i], "_", 2)
			context = karma.Describe("spec", specs[i])
		)

		if len(parts) < 2 {
			return nil, context.Format(
				err,
				"{options} option spec does not contain separator '_'",
			)
		}

		switch parts[0] {
		case "w":
			options.Width, err = NewOptionSize(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{options} unable to parse width option",
				)
			}

		case "h":
			options.Height, err = NewOptionSize(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{options} unable to parse height option",
				)
			}

		case "q":
			options.Quality, err = NewOptionInt(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{options} unable to parse quality option",
				)
			}

		case "c":
			options.Crop, err = NewOptionCrop(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{options} unable to parse crop option",
				)
			}

		case "g":
			options.Gravity, err = NewOptionGravity(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{option} unable to parse gravity option",
				)
			}

		case "r":
			options.Radial, err = NewOptionRadial(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{option} unable to parse radial option",
				)
			}

		case "bo":
			options.Border, err = NewOptionBorder(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{option} unable to parse border option",
				)
			}

		case "l":
			options.Watermark, err = NewOptionWatermark(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{option} unable to parse watermark option",
				)
			}

		case "bg":
			options.Background, err = NewOptionBackground(parts[1])
			if err != nil {
				return nil, context.Format(
					err,
					"{option} unable to parse background option",
				)
			}
		}
	}

	return &options, nil
}
