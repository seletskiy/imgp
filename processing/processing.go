package processing

import (
	"image"
	"io"
	"math"
	"os"

	"github.com/kovetskiy/lorg"
	"github.com/nfnt/resize"
	"github.com/reconquest/karma-go"

	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
)

type OutputFormat string

func (format OutputFormat) String() string {
	return string(format)
}

const (
	OutputFormatPNG  OutputFormat = "png"
	OutputFormatJPG               = "jpg"
	OutputFormatJPEG              = "jpeg"
)

// Processing represents processing process of single image with given options.
type Processing struct {
	Image          *image.RGBA
	Options        Options
	Watermarks     map[string]*Watermark
	FaceRecognizer FaceRecognizer
	Log            lorg.Logger
	Format         OutputFormat

	state struct {
		input struct {
			width  int
			height int
		}

		output struct {
			width  int
			height int
		}

		gravity struct {
			center struct {
				x int
				y int
			}

			delta struct {
				x int
				y int
			}
		}

		quality int

		background color.Color

		image image.Image
	}
}

type Watermark struct {
	X     int
	Y     int
	Image image.Image
}

func New(
	options Options,
	recognizer FaceRecognizer,
	log *lorg.Log,
) *Processing {
	return &Processing{
		FaceRecognizer: recognizer,
		Options:        options,
		Log:            log.NewChildWithPrefix("{processing}"),
		Watermarks:     map[string]*Watermark{},
	}
}

func (processing *Processing) LoadFile(path string) error {
	processing.Log.Debugf("loading file: %q", path)

	file, err := os.Open(path)
	if err != nil {
		return karma.Format(
			err,
			"{processing} unable to open image file: %q",
			path,
		)
	}

	input, _, err := image.Decode(file)
	if err != nil {
		return karma.Format(
			err,
			"{processing} unable to decode image file: %q",
			path,
		)
	}

	processing.SetImage(input)

	return nil
}

func (processing *Processing) SetImage(input image.Image) {
	rgba := image.NewRGBA(input.Bounds())

	for x := 0; x < input.Bounds().Dx(); x++ {
		for y := 0; y < input.Bounds().Dy(); y++ {
			r, g, b, a := input.At(x, y).RGBA()

			rgba.SetRGBA(x, y, color.RGBA{
				uint8(r >> 8),
				uint8(g >> 8),
				uint8(b >> 8),
				uint8(a >> 8),
			})
		}
	}

	processing.Image = rgba
}

func (processing *Processing) SetFormat(format string) {
	processing.Format = OutputFormat(format)
}

func (processing *Processing) Process(writer io.Writer) error {
	processing.setBackground()
	processing.setSize()
	processing.setQuality()
	processing.setGravity()

	processing.scale()
	processing.watermark()

	err := processing.write(writer)
	if err != nil {
		return err
	}

	return nil
}

func (processing *Processing) setBackground() {
	if processing.Options.Background != nil {
		processing.state.background = *processing.Options.Background
		if *processing.Options.Background == OptionBackgroundTransparent {
			processing.Format = OutputFormatPNG
		}
	} else {
		processing.state.background = color.White
	}
}

func (processing *Processing) setSize() {
	var (
		options = processing.Options
		input   = processing.Image
	)

	var (
		width  int
		height int
	)

	bounds := input.Bounds()

	state := processing.state

	state.input.width = bounds.Dx()
	state.input.height = bounds.Dy()

	if options.Width != nil {
		if options.Width.Relative() {
			width = int(
				float64(state.input.width) * options.Width.Float64(),
			)
		} else {
			width = int(options.Width.Int64())
		}
	}

	if options.Height != nil {
		if options.Height.Relative() {
			height = int(
				float64(state.input.height) * options.Height.Float64(),
			)
		} else {
			height = int(options.Height.Int64())
		}
	}

	switch {
	case width == 0 && height == 0:
		width, height = bounds.Dx(), bounds.Dy()

	case width == 0:
		width = int(
			float64(height) * float64(bounds.Dx()) / float64(bounds.Dy()),
		)

	case height == 0:
		height = int(
			float64(width) * float64(bounds.Dy()) / float64(bounds.Dx()),
		)
	}

	state.output.width = width
	state.output.height = height

	processing.state = state
}

func (processing *Processing) setQuality() {
	var (
		options = processing.Options
	)

	var quality int

	if options.Quality != nil {
		quality = int(options.Quality.Int64())
	} else {
		quality = 100
	}

	processing.Log.Debugf("target JPEG quality: %d", quality)

	processing.state.quality = quality
}

func (processing *Processing) setGravity() {
	processing.setGravityCenter()

	if processing.Options.Gravity != nil {
		switch *processing.Options.Gravity {
		case OptionGravityFace:
			processing.setGravityFace()
		}
	}
}

func (processing *Processing) setGravityCenter() {
	state := processing.state

	state.gravity.center.x = state.input.width / 2
	state.gravity.center.y = state.input.height / 2
	state.gravity.delta.x = min(state.gravity.center.x, state.gravity.center.y)
	state.gravity.delta.y = state.gravity.delta.x

	processing.state = state
}

func (processing *Processing) setGravityFace() {
	processing.Log.Debugf("detecting face")

	faces := processing.FaceRecognizer.Recognize(processing.Image)
	if len(faces) == 0 {
		return
	}

	var face image.Rectangle

	for _, candidate := range faces {
		if candidate.Dx()*candidate.Dy() > face.Dx()*face.Dy() {
			face = candidate

			processing.Log.Debugf("detected face: %v", face)
		}
	}

	// magic constants taken from original code and tuned for used face
	// recognition libs
	resizeFactor := 0.20
	eyesPadFactor := -0.1

	padX := int(float64(face.Dx()) * resizeFactor)
	padY := int(float64(face.Dy()) * resizeFactor)

	eyesPad := int(float64(face.Dy()) * eyesPadFactor)

	box := image.Rect(
		max(0, face.Min.X-padX),
		max(0, face.Min.Y-padY-eyesPad),
		min(processing.Image.Bounds().Dx(), face.Max.X+padX),
		min(processing.Image.Bounds().Dy(), face.Max.Y+padY-eyesPad),
	)

	state := processing.state

	state.gravity.center.x = box.Min.X + box.Dx()/2
	state.gravity.center.y = box.Min.Y + box.Dy()/2
	state.gravity.delta.x = box.Dx() / 2
	state.gravity.delta.y = box.Dy() / 2

	processing.state = state
}

func (processing *Processing) scale() {
	if processing.Options.Crop == nil {
		processing.resize()
	} else {
		switch *processing.Options.Crop {
		case OptionCropThumb:
			processing.cropThumb()
		case OptionCropFill:
			processing.cropPad(true)
		case OptionCropPad:
			processing.cropPad(false)
		case OptionCropCrop:
			processing.crop()
		}
	}

	if processing.Options.Radial != nil {
		processing.cropRadial()
	} else {
		if processing.Options.Border != nil {
			processing.addRectBorder()
		}
	}
}

func (processing *Processing) watermark() {
	if processing.Options.Watermark != nil {
		watermark := processing.Watermarks[processing.Options.Watermark.Name()]

		if watermark != nil {
			processing.addWatermark(watermark)
		}
	}
}

func (processing *Processing) resize() {
	processing.Log.Debugf("resize: %v", processing.state.output)

	processing.state.image = resize.Resize(
		uint(processing.state.output.width),
		uint(processing.state.output.height),
		processing.Image,
		resize.Lanczos3,
	)
}

func (processing *Processing) cropThumb() {
	state := processing.state

	ratio := float64(state.output.width) / float64(state.output.height)

	crop := image.Rectangle{}

	gravity := state.gravity

	crop.Min.X = gravity.center.x - int(float64(gravity.delta.x)*ratio)
	crop.Min.Y = gravity.center.y - int(float64(gravity.delta.y)/ratio)
	crop.Max.X = gravity.center.x + int(float64(gravity.delta.x)*ratio)
	crop.Max.Y = gravity.center.y + int(float64(gravity.delta.y)/ratio)

	processing.Log.Debugf("crop thumb: %v", crop)

	thumb := processing.Image.SubImage(crop)

	factor := math.Max(
		float64(state.output.width)/float64(crop.Dx()),
		float64(state.output.height)/float64(crop.Dy()),
	)

	size := image.Point{
		int(float64(crop.Dx()) * factor),
		int(float64(crop.Dy()) * factor),
	}

	processing.Log.Debugf("resize: %v", size)

	state.image = resize.Resize(
		uint(size.X),
		uint(size.Y),
		thumb,
		resize.Lanczos3,
	)

	processing.state = state
}

func (processing *Processing) crop() {
	state := processing.state

	crop := image.Rectangle{}

	gravity := state.gravity

	crop.Min.X = max(0, gravity.center.x-state.output.width/2)
	crop.Min.Y = max(0, gravity.center.y-state.output.height/2)
	crop.Max.X = gravity.center.x + state.output.width/2
	crop.Max.Y = gravity.center.y + state.output.width/2

	processing.Log.Debugf("crop: %v", crop)

	thumb := processing.Image.SubImage(crop)

	state.image = thumb

	processing.state = state
}

func (processing *Processing) cropFill() {
	state := processing.state

	factor := math.Min(
		float64(state.input.width)/float64(state.output.width),
		float64(state.input.height)/float64(state.output.height),
	)

	size := image.Point{
		int(float64(state.input.width) / factor),
		int(float64(state.input.height) / factor),
	}

	processing.Log.Debugf("crop fill: %v", size)

	state.image = resize.Resize(
		uint(size.X),
		uint(size.Y),
		processing.Image,
		resize.Lanczos3,
	)

	processing.state = state
}

func (processing *Processing) cropPad(min bool) {
	state := processing.state

	var factor float64

	if min {
		factor = math.Min(
			float64(state.input.width)/float64(state.output.width),
			float64(state.input.height)/float64(state.output.height),
		)
	} else {
		factor = math.Max(
			float64(state.input.width)/float64(state.output.width),
			float64(state.input.height)/float64(state.output.height),
		)
	}

	size := image.Point{
		int(float64(state.input.width) / factor),
		int(float64(state.input.height) / factor),
	}

	processing.Log.Debugf("crop pad: %v", size)

	state.image = resize.Resize(
		uint(size.X),
		uint(size.Y),
		processing.Image,
		resize.Lanczos3,
	)

	bg := image.NewRGBA(
		image.Rect(
			0, 0,
			state.output.width,
			state.output.height,
		),
	)

	fill(bg, state.background)

	pivot := image.Point{
		(state.output.width - size.X) / 2,
		(state.output.height - size.Y) / 2,
	}

	draw.Draw(
		bg,
		image.Rect(
			pivot.X,
			pivot.Y,
			pivot.X+size.X,
			pivot.Y+size.Y,
		),
		state.image,
		image.ZP,
		draw.Src,
	)

	state.image = bg

	processing.state = state
}

func (processing *Processing) cropRadial() {
	state := processing.state

	bounds := state.image.Bounds()

	width, height := bounds.Dx(), bounds.Dy()

	var radiusX int
	var radiusY int

	if *processing.Options.Radial == OptionRadialMax {
		radiusX = width / 2
		radiusY = height / 2
	} else {
		radiusX = *processing.Options.Radial
		radiusY = *processing.Options.Radial
	}

	processing.Log.Debugf(
		"crop radial: %v | radius (%d, %d)",
		bounds,
		radiusX,
		radiusY,
	)

	masked := image.NewRGBA(image.Rect(0, 0, bounds.Dx(), bounds.Dy()))

	fill(masked, state.background)

	draw.DrawMask(
		masked,
		masked.Bounds(),
		state.image,
		bounds.Min,
		&RoundedRectangle{
			Center:  image.Pt(width/2, height/2),
			Width:   width,
			Height:  height,
			RadiusX: radiusX,
			RadiusY: radiusY,
		},
		image.ZP,
		draw.Over,
	)

	state.image = masked

	processing.state = state

	processing.addRadialBorder(radiusX, radiusY)
}

func (processing *Processing) addRadialBorder(radiusX, radiusY int) {
	if processing.Options.Border == nil {
		return
	}

	border := *processing.Options.Border

	state := processing.state

	bounds := state.image.Bounds()
	bounds.Max.X += int(border.Width) * 2
	bounds.Max.Y += int(border.Width) * 2

	masked := image.NewRGBA(bounds)
	fill(masked, state.background)

	width, height := bounds.Dx(), bounds.Dy()

	draw.DrawMask(
		masked,
		masked.Bounds(),
		image.NewUniform(border.Color),
		image.ZP,
		&RoundedRectangle{
			Center:  image.Pt(width/2, height/2),
			Width:   width,
			Height:  height,
			RadiusX: radiusX,
			RadiusY: radiusY,
		},
		image.ZP,
		draw.Over,
	)

	pivot := masked.Bounds()
	pivot.Min.X += int(border.Width)
	pivot.Min.Y += int(border.Width)
	pivot.Max.X -= int(border.Width)
	pivot.Max.Y -= int(border.Width)

	draw.DrawMask(
		masked,
		pivot,
		state.image,
		image.ZP,
		&RoundedRectangle{
			Center: image.Pt(
				width/2-int(border.Width),
				height/2-int(border.Width),
			),
			Width:   width - int(border.Width)*2,
			Height:  height - int(border.Width)*2,
			RadiusX: radiusX,
			RadiusY: radiusY,
		},
		image.ZP,
		draw.Over,
	)

	state.image = masked

	processing.state = state
}

func (processing *Processing) addRectBorder() {
	state := processing.state
	border := processing.Options.Border

	bounds := state.image.Bounds()
	bounds.Max.X += int(border.Width) * 2
	bounds.Max.Y += int(border.Width) * 2

	masked := image.NewRGBA(bounds)
	fill(masked, state.background)

	draw.Draw(
		masked,
		image.Rect(
			int(border.Width),
			int(border.Width),
			int(border.Width)+state.image.Bounds().Dx(),
			int(border.Width)+state.image.Bounds().Dy(),
		),
		state.image,
		image.ZP,
		draw.Src,
	)

	state.image = masked

	processing.state = state
}

func (processing *Processing) addWatermark(mark *Watermark) {
	state := processing.state

	x := mark.X
	y := mark.Y

	if x < 0 {
		x = state.image.Bounds().Dx() + x - mark.Image.Bounds().Dx()
	}

	if y < 0 {
		y = state.image.Bounds().Dy() + y - mark.Image.Bounds().Dy()
	}

	var source draw.Image

	switch image := state.image.(type) {
	case *image.RGBA:
		source = image
	}

	draw.Draw(
		source,
		image.Rect(
			x, y,
			x+mark.Image.Bounds().Dx(), y+mark.Image.Bounds().Dy(),
		),
		mark.Image,
		image.ZP,
		draw.Over,
	)

	processing.state = state
}

func (processing *Processing) write(writer io.Writer) error {
	switch processing.Format {
	case OutputFormatPNG:
		return processing.writePNG(writer)
	default:
		return processing.writeJPG(writer)
	}
}

func (processing *Processing) writeJPG(writer io.Writer) error {
	err := jpeg.Encode(
		writer,
		processing.state.image,
		&jpeg.Options{
			Quality: processing.state.quality,
		},
	)
	if err != nil {
		return karma.Format(
			err,
			"{processing} unable to encode jpeg image",
		)
	}

	return nil
}

func (processing *Processing) writePNG(writer io.Writer) error {
	err := png.Encode(
		writer,
		processing.state.image,
	)
	if err != nil {
		return karma.Format(
			err,
			"{processing} unable to encode png image",
		)
	}

	return nil
}
