package processing

import "fmt"

type OptionCrop string

const (
	OptionCropThumb OptionCrop = "thumb"
	OptionCropFill             = "fill"
	OptionCropPad              = "pad"
	OptionCropCrop             = "crop"
)

func NewOptionCrop(spec string) (*OptionCrop, error) {
	known := map[OptionCrop]bool{
		OptionCropThumb: true,
		OptionCropFill:  true,
		OptionCropPad:   true,
		OptionCropCrop:  true,
	}

	option := OptionCrop(spec)

	if !known[option] {
		return nil, fmt.Errorf(
			"{option:crop} unknown crop type: %s",
			spec,
		)
	}

	return &option, nil
}
