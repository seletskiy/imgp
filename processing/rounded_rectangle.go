package processing

import (
	"image"
	"image/color"
	"math"
)

type RoundedRectangle struct {
	Center  image.Point
	Width   int
	Height  int
	RadiusX int
	RadiusY int
}

func (rect *RoundedRectangle) ColorModel() color.Model {
	return color.AlphaModel
}

func (rect *RoundedRectangle) Bounds() image.Rectangle {
	return image.Rect(
		rect.Center.X-rect.Width,
		rect.Center.Y-rect.Height,
		rect.Center.X+rect.Width,
		rect.Center.Y+rect.Height,
	)
}

func (rect *RoundedRectangle) At(x, y int) color.Color {
	width := rect.Width/2 - rect.RadiusX

	if x >= rect.Center.X-width && x <= rect.Center.X+width {
		if y >= rect.Center.Y-rect.Height && y <= rect.Center.Y+rect.Height {
			return color.Alpha{255}
		}
	}

	height := rect.Height/2 - rect.RadiusY

	if y >= rect.Center.Y-height && y <= rect.Center.Y+height {
		if x >= rect.Center.X-rect.Width && x <= rect.Center.X+rect.Width {
			return color.Alpha{255}
		}
	}

	for _, corner := range []struct {
		x int
		y int
	}{
		{-1, -1},
		{+1, -1},
		{-1, +1},
		{+1, +1},
	} {
		center := image.Pt(
			rect.Center.X+corner.x*width,
			rect.Center.Y+corner.y*height,
		)

		if math.Signbit(float64(x-center.X)) != math.Signbit(float64(corner.x)) {
			continue
		}

		if math.Signbit(float64(y-center.Y)) != math.Signbit(float64(corner.y)) {
			continue
		}

		corner := Ellipse{
			Center:  center,
			RadiusX: rect.RadiusX,
			RadiusY: rect.RadiusY,
		}

		if _, _, _, a := corner.At(x, y).RGBA(); a > 0 {
			return color.Alpha{uint8(a >> 8)}
		}
	}

	return color.Alpha{0}
}
