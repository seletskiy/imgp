# imgp

Image Processor.

# Installation

## Requirements

Project optionally uses OpenCV for face recognition, so it must be
pre-installed on system if you want to use OpenCV for face recognition.

## Setup

```
go get gitlab.com/seletskiy/imgp
```

## Building with OpenCV

Specify `opencv` build tag to get binary with OpenCV face recognition instead
of Golang based:

```
go build -tags opencv
```

# Example usage

Run `imgp` from project directory:

To run with Golang based face recognition

```
./imgp --classifier classifiers/pigo-facefinder --spec w_300,h_600,c_pad input.jpg output.jpg
```

To run with OpenCV recognition:

```
./imgp --classifier classifiers/opencv-haarcascade-frontalface.xml --spec w_300,h_600,c_pad input.jpg output.jpg
```
