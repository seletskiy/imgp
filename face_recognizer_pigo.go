// +build !opencv

package main

import (
	"gitlab.com/seletskiy/imgp/processing"
)

func NewFaceRecognizer(classifier string) (processing.FaceRecognizer, error) {
	return processing.NewFaceRecognizerPigo(classifier)
}
