package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"image"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gitlab.com/seletskiy/imgp/processing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	_ "image/jpeg"
	_ "image/png"
)

var (
	reQueryString = regexp.MustCompile(`^([^/]+)/(.+)`)
)

type Handler struct {
	S3         *s3.S3
	BucketName string
	BucketURL  string
	Recognizer processing.FaceRecognizer
	Presets    map[string]string
	Watermarks map[string]*processing.Watermark

	PresetsOnly bool
}

func (handler *Handler) HandleLambda(
	ctx context.Context,
	request events.APIGatewayProxyRequest,
) (events.APIGatewayProxyResponse, error) {
	key := request.QueryStringParameters["key"]
	if key == "" {
		log.Errorf(
			"query string parameter 'key' is not set: %v",
			request.QueryStringParameters,
		)

		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, nil
	}

	code, headers, body := handler.Handle(key)
	if headers == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: code,
		}, nil
	}

	if body != nil {
		return events.APIGatewayProxyResponse{
			IsBase64Encoded: true,
			Body:            base64.StdEncoding.EncodeToString(body),
			Headers:         headers,
			StatusCode:      code,
		}, nil
	}

	return events.APIGatewayProxyResponse{
		StatusCode: code,
		Headers:    headers,
	}, nil
}

func (handler *Handler) HandleHTTP(
	writer http.ResponseWriter,
	request *http.Request,
) {
	key := request.URL.Query().Get("key")
	if key == "" {
		log.Errorf(
			"query string parameter 'key' is not set: %v",
			request.URL.Query(),
		)

		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	code, headers, body := handler.Handle(key)
	if headers == nil {
		writer.WriteHeader(code)
		return
	}

	for name, value := range headers {
		writer.Header().Add(name, value)
	}

	if body != nil {
		writer.WriteHeader(code)
		writer.Write(body)

		return
	}

	writer.WriteHeader(code)
}

func (handler *Handler) Handle(
	key string,
) (int, map[string]string, []byte) {
	matches := reQueryString.FindStringSubmatch(key)
	if len(matches) == 0 {
		log.Errorf(
			"query string parameter 'key' is malformed: %v",
			key,
		)

		return 400, nil, nil
	}

	var (
		spec = matches[1]
		file = matches[2]
	)

	var preset bool

	if _, ok := handler.Presets[spec]; ok {
		log.Infof("using preset: %q", spec)

		preset = true
		spec = handler.Presets[spec]
	}

	if !preset && handler.PresetsOnly {
		return 403, nil, nil
	}

	log.Infof(
		"processing request: file %q | spec %q",
		file,
		spec,
	)

	started := time.Now()

	object, err := handler.S3.GetObject(
		&s3.GetObjectInput{
			Bucket: aws.String(handler.BucketName),
			Key:    aws.String(file),
		},
	)
	if err != nil {
		if err, ok := err.(awserr.Error); ok {
			if err.Code() == s3.ErrCodeNoSuchKey {
				return 404, nil, nil
			}
		}

		log.Errorf("unexpected error from S3 while retrieving file: %s", err)

		return 500, nil, nil
	}

	body, err := ioutil.ReadAll(object.Body)
	if err != nil {
		log.Error(
			"unexpected error while from S3 while reading file body: %s",
			err,
		)

		return 500, nil, nil
	}

	log.Infof(
		"get object from s3 took: %.4fs",
		time.Now().Sub(started).Seconds(),
	)

	started = time.Now()

	input, format, err := image.Decode(bytes.NewBuffer(body))
	if err != nil {
		log.Errorf("unable decode image file from S3: %s", err)

		return 500, nil, nil
	}

	log.Infof("source format: %s", format)

	log.Infof(
		"image decode took: %.4fs",
		time.Now().Sub(started).Seconds(),
	)

	started = time.Now()

	options, err := processing.NewOptions(spec)
	if err != nil {
		log.Errorf("malformed processing spec received: %s", err)

		return 400, nil, nil
	}

	format = strings.TrimPrefix(filepath.Ext(file), ".")

	processing := processing.New(*options, handler.Recognizer, log)
	processing.Watermarks = handler.Watermarks
	processing.SetImage(input)
	processing.SetFormat(format)

	var buffer bytes.Buffer
	err = processing.Process(&buffer)
	if err != nil {
		log.Errorf("error while processing image: %s", err)

		return 500, nil, nil
	}

	log.Infof(
		"processing took: %.4fs",
		time.Now().Sub(started).Seconds(),
	)

	if !preset {
		headers := map[string]string{"Content-Type": "image/" + format}
		return 200, headers, buffer.Bytes()
	}

	key = strings.TrimSuffix(key, format) + processing.Format.String()

	started = time.Now()

	manager := s3manager.NewUploaderWithClient(handler.S3)
	_, err = manager.Upload(
		&s3manager.UploadInput{
			Bucket:      aws.String(handler.BucketName),
			Key:         aws.String(key),
			ContentType: aws.String("image/" + format),
			Body:        &buffer,
		},
	)
	if err != nil {
		log.Errorf("unable to put object to s3: %s", err)

		return 500, nil, nil
	}

	log.Infof(
		"put object to s3 took: %.4fs",
		time.Now().Sub(started).Seconds(),
	)

	headers := map[string]string{
		"Location": strings.TrimSuffix(handler.BucketURL, "/") + "/" + key,
	}

	return 301, headers, nil
}
