# Using in EC2 instance

EC2 instance should be configured to have full access to target bucket via IAM.

Build ZIP file:

```
make build
```

Upload ZIP archive to target EC2 instance, unpack it.

Run:

```
sudo BUCKET=<bucket-name> URL=<bucket-http-address> AWS_REGION=<bucket-region> ./main --http :80
```

For example:

```
sudo BUCKET=imgp-upwork URL=http://imgp-upwork.s3-website.eu-central-1.amazonaws.com AWS_REGION=eu-central-1 ./main --http :80
```

# Using in AWS Lambda

First, build ZIP file with lambda handler:

```
make build
```

Then follow instructions described in [AWS blog][1].

In the end, instead of specifying target image size in URL like `300x300` use
spec strings like `c_thumb,w_150,h_150,g_face`.

# Configuration

Lambda handler can be configured using:

* environment variables;
* `presets.json` file;
* `watermarks` directory;

## Environment variables

Following environment variables supported:

* `BUCKET`: specify S3 bucket name which should be used to read original
  images from and to put resized images to;
* `URL`: base URL to S3 bucket to redirect request to after resizing;
* `PRESETS_ONLY`: disable raw commands and allow presets only;

## Presets (`presets.json`)

Presets should be specified in `presets.json` file in zip-archive with lambda
handler and should have following format:

```
{
    "<preset name>": "<raw command>",
    ...
}
```

## Watermarks (`watermarks` directory)

Watermarks can be placed on result image using `l_<watermark name>` raw command.
Watermarks are read from `watermarks` directory and each watermark is defined
by two files with same base name but different extensions, e.g.:

```
preview.png
preview.json
```

`.json` file should contain target watermark position in following format:

```
{
    "x": "<x position>",
    "y": "<y position>"
}
```

Positions can be specified negative: in that case watermark will be aligned
relative to right/bottom side of image.

# Disabling raw commands

Add non-empty `PRESETS_ONLY` to environment variable on AWS Lambda
configuration page.

[1]: https://aws.amazon.com/blogs/compute/resize-images-on-the-fly-with-amazon-s3-aws-lambda-and-amazon-api-gateway/
