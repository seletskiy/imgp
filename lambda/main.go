package main

import (
	"encoding/json"
	"image"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/docopt/docopt-go"
	"github.com/kovetskiy/lorg"
	"gitlab.com/seletskiy/imgp/processing"
)

var version = "1.0"

var usage = `lambda - imgp server (AWS Lambda or HTTP).

Usage:
  lambda -h | --help
  lambda
  lambda --http <listen>

Options:
  -h --help  Show this help.
  --http     Run HTTP server instead of Lambda.
`

var (
	log *lorg.Log
)

func main() {
	args, err := docopt.Parse(usage, nil, true, "lambda "+version, false)
	if err != nil {
		panic(err)
	}

	log = lorg.NewLog()

	log.SetLevel(lorg.LevelDebug)
	log.SetIndentLines(true)
	log.SetFormat(lorg.NewFormat(
		`${time:2006-01-02T15:04:05Z07:00.000} ${level:[%s]\::right:true} ${prefix}%s`,
	))

	handler := &Handler{
		Watermarks: map[string]*processing.Watermark{},
	}

	handler.BucketName = os.Getenv("BUCKET")
	if handler.BucketName == "" {
		log.Fatalf("BUCKET environment variable is not set")
	}

	handler.BucketURL = os.Getenv("URL")
	if handler.BucketURL == "" {
		log.Fatalf("URL environment variable is not set")
	}

	if os.Getenv("PRESETS_ONLY") != "" {
		handler.PresetsOnly = true
	}

	log.Infof("$BUCKET: %s", handler.BucketName)
	log.Infof("$URL: %s", handler.BucketURL)

	handler.Recognizer, err = processing.NewFaceRecognizerPigo("classifier")
	if err != nil {
		log.Fatalf("unable to load classifier: %s", err)
	}

	presets, err := os.Open("presets.json")
	if err != nil {
		log.Fatalf("unable to open presets file: %s", err)
	}

	err = json.NewDecoder(presets).Decode(&handler.Presets)
	if err != nil {
		log.Fatalf("unable to decode presets file: %s", err)
	}

	loadWatermarks(handler)

	handler.S3 = s3.New(session.New())

	if args["--http"].(bool) {
		listen := args["<listen>"].(string)
		if listen == "" {
			listen = ":80"
		}

		if os.Getenv("AWS_REGION") == "" {
			log.Fatalf("AWS_REGION environment variable is not set")
		}

		http.HandleFunc("/", handler.HandleHTTP)
		log.Fatal(http.ListenAndServe(listen, nil))
	} else {
		lambda.Start(handler.HandleLambda)
	}
}

func loadWatermarks(handler *Handler) {
	filepath.Walk(
		"watermarks",
		func(path string, info os.FileInfo, err error) error {
			if !strings.HasSuffix(path, ".json") {
				return nil
			}

			watermarkSpec, err := os.Open(path)
			if err != nil {
				log.Errorf("unable to open watermark file: %s", err)
			} else {
				var watermark processing.Watermark

				err = json.NewDecoder(watermarkSpec).Decode(&watermark)
				if err != nil {
					log.Fatalf("unable to decode watermark file: %s", err)
				}

				filename := strings.TrimSuffix(path, ".json")

				watermarkFile, err := os.Open(filename + ".png")
				if err != nil {
					log.Fatalf("unable to open watermark image: %s", err)
				}

				watermark.Image, _, err = image.Decode(watermarkFile)
				if err != nil {
					log.Fatal(err)
				}

				handler.Watermarks[filepath.Base(filename)] = &watermark
			}

			return nil
		},
	)
}
